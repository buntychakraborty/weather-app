Weather Application:
This is basically a weather application showing the weather of any location you want.

Technologies used to develop this application:

1.Spring Boot
2.Thymeleaf
3.Spring RestFul Services
4.Java 8 and Java 9
5.HTML Geolocation Concepts

Installation Steps:<br/>
Way 1: <br/>
     1. git clone git@github.com:buntychakraborty/weather-app.git <br/>
2.mvn clean install  <br/>
3.docker build -t weather-app .  <br/>
4.docker run -td --name weather-app -p 9098:9098 weather-app <br/>

Go to the browser and type http://localhost:9098

Way 2:
1.docker pull bunty09/weather-app
2.docker run -td --name weather-app -p 9098:9098 bunty09/weather-app

Go to the browser and type http://localhost:9098


You can see the live demo here: https://weather-app-climate.herokuapp.com/

<br/>



![Screenshot 2021-09-21 at 18-17-54 Weather Application](https://user-images.githubusercontent.com/20379210/134173585-2ac1a79f-5149-4998-ae71-09550669b2e5.png)




