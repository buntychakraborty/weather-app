FROM openjdk:8
ADD target/weather-app.jar weather-app
EXPOSE 9098
ENTRYPOINT ["java","-jar","weather-app"]
